const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');
const UserSeeder = require('../seeder/UserSeeder');

router.post('/addUser', function (req, res) {
	UserController.create(req, res);
});

router.get('/getUsers', function (req, res) {
	UserController.list(req, res);
});

router.get('/seedUsers', function (req, res) {
	UserSeeder.seedUsers(req, res);
});

router.post('/login', function (req, res) {
	UserController.login(req, res);
});

module.exports = router;
