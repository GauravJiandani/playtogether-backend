const mongoose = require('mongoose');

const MONGO_USERNAME = 'user';
const MONGO_PASSWORD = 'password';
const MONGO_HOSTNAME = '127.0.0.1';
const MONGO_PORT = '27017';
const MONGO_DB = 'mydb';

// const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;
const url = "mongodb://localhost:27017/mydb";

mongoose.connection.on("open", function (ref) {
	console.log("Connected to mongo server.");
});

mongoose.connection.on("error", function (err) {
	console.log("Could not connect to mongo server!");
	return console.log(err);
});

mongoose.connect(url);
