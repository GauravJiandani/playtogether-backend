const User = require("../models/user");
const fs = require("fs");

exports.seedUsers = function(req, res) {
	let jsonData = {};
	fs.readFile(__dirname + "/UserSeeder.json", "utf-8", (err, data) => {
		jsonData = JSON.parse(data);
		jsonData.user.forEach(element => {
			var newUser = new User(element);
			newUser.save(function(err) {
				if (err) {
					console.log(err);
				}
			});
		});
		res.end();
	});
};
