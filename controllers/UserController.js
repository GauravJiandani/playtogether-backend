const User = require('../models/user');

exports.create = function (req, res) {
	var newUser = new User(req.body);
	newUser.save(function (err) {
		if (err) {
			res.status(400).send('Unable to save User to database');
		} else {
			res.status(200).send('Created');
		}
	});
};

exports.list = function (req, res) {
	User.find({}).exec(function (err, users) {
		if (err) {
			return res.status(500).send(err);
		}
		res.status(200).send(
			{
				users: users
			}
		);
	});
};

exports.login = function (req, res) {
	var email = req.body.email;
	var password = req.body.password;
	User.findOne({ 'email': email, 'password': password }, function (err, user) {
        if (err) {
            res.status(500).send(err);
        }
        if (user) {
			res.status(200).send({'msg': 'Login successfull'});
		}
	});
	res.end();
};
