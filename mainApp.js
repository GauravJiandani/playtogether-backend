const express = require("express");
const app = express();
var cors = require("cors");
app.options("*", cors());
app.use(cors({
	origin: (o, cb) => cb(null, true)
}));
const router = express.Router();
const db = require("./db");
const routes = require("./routes/index");

const port = 4000;

app.use(express.urlencoded({ extended: true }));
app.use("/api", routes);

app.listen(port, function() {
  console.log("Example app listening on port 4000");
});
